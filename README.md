# docker-vertx-exercises

DockerとVert.xの練習問題

----

## このプロジェクトをフォークする

自分用に、このプロジェクトをフォークする。フォークしたプロジェクトは、「設定-リポジトリの概要-アクセスレベル」で非公開設定とする。

----

## Dockerの環境を整える

* Docker toolsを導入する

「[Windowsへの導入](https://docs.docker.com/installation/windows/)」や「[Macへの導入](https://docs.docker.com/installation/mac/)」を参考に、Docker toolsを導入する。他の方法で、Docker環境が作れる場合は、以下の文書を適時読み替えても構わない。

* Dockerを動かしてみる

`# docker run hello-world`

練習課題（１）：実行したターミナルのスクリーンショットを、ここに貼る

----

## Vert.xのサンプルを動かしてみる

* dockerを動かすマシンに入る

docker-machineを使っている場合、実際にはリモートでdockerコマンドが実行できる環境が用意されているだけなので、「[docker-machine ssh](https://docs.docker.com/machine/reference/ssh/)」を使って、dockerを動かすマシンに入る。

* サンプルを実行する

```
# git clone https://bitbucket.org/mnagaku/docker-vertx-exercises.git
# cd docker-vertx-exercises
# docker run --rm -v "$PWD:/build" maven /bin/sh -c 'cd /build && mvn package'
# docker run --rm -p 80:80 -v "$PWD/target:/target" vertx/vertx3 vertx run HelloWorldVerticle -cp /target/maven-verticle-3.0.0.jar
```

ctrl+cで停止させるまで、サンプルのサーバが動作しているので、Webブラウザからアクセスして、レスポンスが返ることを確認する。docker-machineを使っている場合、アクセスするIPアドレスは、「[docker-machine ip](https://docs.docker.com/machine/reference/ip/)」で確認できる。

練習課題（２）：Webブラウザから、サンプルのサーバに3回アクセスした後の、サーバを実行しているターミナルのスクリーンショットを、ここに貼る

----

## Vert.xのサンプルを改造してみる

サンプルは、訪問者カウンタとなっているが、エラい人から、

「どうせなら特別な番号を踏んだ方が、訪問者はうれしいよね！そうだ！カウンターが素数だけでカウントアップしていくってどう？全ての訪問者が特別な気分を味わえるように！」

という変更要求がでた。そこで、HelloWorldVerticle.javaを改造して、カウンター表示が素数だけでカウントアップしていくようにする。改造に際しては、クラスの追加やリファクタリングなど制限を設けない。

練習課題（３）：改造したコードを、フォークした自分のプロジェクトにコミットする

----

## コンテナを作ってみる

* Docker Hubにアカウントを作る

作ったコンテナをpushできるようにDocker Hubのアカウントを作る。以下、Docker Hubのアカウント名、Bitbucketのアカウント名は「your_name」としてコマンドなどを記述するので、適時、自分のアカウント名で読み替えること。

* コンテナをビルドする

リポジトリにあるDockerfileを使って、HelloWorldVerticleが同梱されたコンテナをビルドする。

```
# git clone https://your_name@bitbucket.org/your_name/docker-vertx-exercises.git
# cd docker-vertx-exercises
# docker run --rm -v "$PWD:/build" maven /bin/sh -c 'cd /build && mvn package'
# docker build -t your_name/docker-vertx-exercises .
```

* コンテナを実行する

```
# docker run -it --name=dve1 -p 80:80 your_name/docker-vertx-exercises
```

Webブラウザで動作を確認する。ctrl+cで停止させたら、停止コンテナが残っているので、

```
# docker rm dve1
```

で消す。

* コンテナをDocker Hubに上げる

Docker Hubに「your_name/docker-vertx-exercises」というプロジェクトをpublicで作り、「[docker push](https://docs.docker.com/reference/commandline/push/)」で上げる。

ローカルのコンテナイメージ「your_name/docker-vertx-exercises」を捨ててから、実行すると、Docker Hubからpullされて動くことが確認できる。

練習課題（４）：Docker Hubにコンテナイメージ「your_name/docker-vertx-exercises」を上げる。プロジェクトへのURLをここに貼る

----

## 考察
練習課題（５）：「Vert.xのサンプルを改造してみる」で体験してもらったように、開発者が試行錯誤を反復的に行いながら開発を進める観点からは、ここに示したワークフローは十分に速いとは言えない。なるべく具体的な改善策を考察せよ（ここに書く）

練習課題（６）：HelloWorldVerticle.javaにおいて、受信待機からレスポンス生成の処理はシングルスレッドで処理される。レスポンス生成の処理が重くなった場合、どのような不具合が起こり得るか考察せよ。また、マルチスレッドで処理されるように改修した場合、新たに別の不具合を起こさないように留意すべき点について考察せよ（ここに書く）

----

## 課題提出

「設定-アクセス管理-ユーザ」に「mnagaku」をread権限で追加することを以って、課題提出とする。
別途、アトミテック側の担当者に、メールにて、プロジェクトのURLを連絡すること。

----

# 参考資料

Docker ハンズオン - 基本コマンド編
http://qiita.com/hihihiroro/items/6dda871dc2566801a6da

Docker
https://docs.docker.com/

Docker - Install
https://docs.docker.com/installation/

Docker Hub
https://hub.docker.com/

Vert.x
http://vertx.io/

Vert.x 3.0 Simple Maven Verticle project
https://github.com/vert-x3/vertx-examples/tree/master/maven-verticles/maven-verticle

Vert.x Stack - Docs 3.0.0 API
http://vertx.io/docs/apidocs/index.html