FROM vertx/vertx3

MAINTAINER mnagaku

ENV DEBIAN_FRONTEND noninteractive

RUN echo "Asia/Tokyo" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata \
    && apt-get update && apt-get upgrade -y && apt-get install -y locales \
    && sed 's/# en_US.UTF-8/en_US.UTF-8/' -i /etc/locale.gen && locale-gen

ADD target /target/

EXPOSE 80

CMD vertx run HelloWorldVerticle -cp /target/maven-verticle-3.0.0.jar
