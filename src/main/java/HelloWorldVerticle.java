import io.vertx.core.AbstractVerticle;

public class HelloWorldVerticle extends AbstractVerticle {
  static int count = 0;
  public void start() {
    vertx.createHttpServer().requestHandler(req -> {
      System.out.printf("["+req.remoteAddress().host()+"]["+req.uri()+"]\n");
      if("/".equals(req.uri()))count++;
      req.response().end("Hello "+req.remoteAddress().host()
        +" ! You are "+count+"th visitor.");
    }).listen(80);
  }
}
